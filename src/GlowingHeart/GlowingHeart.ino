/**
 * Glowing Heart
 * (c)2019 Agis Wichert
 * 
 * http://www.rohling.de
 * https://www.youtube.com/nenioc187 
 */
// Neopixels
#include <Adafruit_NeoPixel.h>
#define NeoPIN 6
#define NUM_LEDS 50
int brightness = 120;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);

// animations
int animationType = 0;
int modeHelper = 0;
int animationCounter = 0;
unsigned long animationPos = 0;
unsigned long animationTime = 250L;
unsigned int pulse = 150;
unsigned long sttu = (60000/pulse);
unsigned long pulseTime = 0L;

// button
#define ModeButton A7
int buttonCycles = 0;
int buttonMaxCycles = 20;
bool isButtonPressed = false;
bool isChanged = false;

uint32_t goldenArray [] = {
  strip.Color(215,255,0), // gold
  strip.Color(215,255,0), // gold1
  strip.Color(201,238,0), // gold2
  strip.Color(173,205,0), // gold3
  strip.Color(117,139,0), // gold4
  strip.Color(165,218,32), // goldenrod
  strip.Color(193,255,37), // goldenrod1
  strip.Color(180,238,34), // goldenrod2
  strip.Color(155,205,29), // goldenrod3
  strip.Color(105,139,20) // goldenrod4
};
int goldenArrayLength = (sizeof(goldenArray)/sizeof(goldenArray[0]));
void setup() {
  // serial output
  Serial.begin(9600);
  Serial.println("====================");
  Serial.println("    Glowing Heart");
  Serial.println("(c)2019 Agis Wichert");
  Serial.println("====================");
  
  // Neopixel
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();

  // timer
  animationPos = millis();
  animationType = 0;
}

void loop() {
  checkMode();
  checkButton();
}

void checkMode(){
  if((animationPos + animationTime) < millis()){
    if(animationType == 0){
      // test animation
      modeTest();
    }
    else if(animationType == 1){
      // random colors
      modeRandom();
    }
    else if(animationType == 3){
      // blinking stars
      modeBlinkingStars();
    }
    else if(animationType == 4){
      // connecting heart
      modeConnect();
    }
    else if(animationType == 5){
      // running mode
      modeRunning();
    }
    else if(animationType == 6){
      // random golden mode
      modeRandomGolden();
    }
    else if(animationType == 7){
      // random golden mode
      modeGoldenShower();
    }
    else if(animationType == 8){
      // gold maker
      modeGoldMaker();
    }
    else if(animationType == 9){
      // random color full
      modeRandomFull();
    }
  }

  // flawless animations
  if(animationType == 2){
    // heart beat
    modeBeatingHeart();
  }
}

// mode 0: Test all pixels sequential
void modeTest(){
  // modeHelper
  int rRed = random(0,254);
  int rBlue = random(0,250);
  int rGreen = random(0,240);
  strip.setPixelColor(modeHelper, strip.Color( rGreen, rRed, rBlue ) );
  strip.show();
  modeHelper++;
  animationPos = millis();
  Serial.print("TestMode ");
  Serial.println(modeHelper, DEC);
  if(modeHelper > NUM_LEDS){
    strip.fill(strip.Color(0,0,0), 0, NUM_LEDS);
    modeHelper = 0;
  }
}

// mode 1: Random color for all LEDS
void modeRandom(){
  for(int i = modeHelper; i < NUM_LEDS; i=i+2){
    int rRed = random(0,254);
    int rBlue = random(0,250);
    int rGreen = random(0,240);
    strip.setPixelColor(i, strip.Color( rGreen, rRed, rBlue ) );
  }
  if(modeHelper == 0){
    modeHelper = 1;
  }
  else {
    modeHelper = 0;
  }
  strip.show();
  animationPos = millis();
  animationCounter++;
}

// mode 2: beating heart simulation
void modeBeatingHeart(){
  //uint32_t magenta = strip.Color(255, 0, 255);
  // 120-200ms P-Welle + PQ-Strecke
  // 100ms herzschlag (QRS-Komplex)
  // (60000/puls)-250ms
  if((pulseTime + 1000L) < millis()){
    // restart
    pulseTime = millis();
    animationCounter++;
  }
  unsigned long moment = millis() - pulseTime;
  if(moment < 100L){
    // P Welle
    strip.fill(strip.Color(0,15,7), 0, NUM_LEDS);
  }
  else if(moment >= 100L && moment <200L){
    // PQ Strecke
    strip.fill(strip.Color(0,0,0), 0, NUM_LEDS);
  }
  else if( moment >= 200L && moment < 300){
    // QRS Komplex (puls)
    strip.fill(strip.Color(0,250,0), 0, NUM_LEDS);
  }
  else if(moment >= sttu ){
    // ST Strecke + T Welle + U Welle
    strip.fill(strip.Color(0,0,0), 0, NUM_LEDS);
  }
  strip.show();
}

// mode 3: 4 pixels randomly white, all other red
void modeBlinkingStars(){
  // all red
  strip.fill(strip.Color(0,250,0), 0, NUM_LEDS);
  for(int i = 0; i < 4; i++){
    int pixi = random(0, NUM_LEDS);
    strip.setPixelColor(pixi, strip.Color( 50, 50, 250 ) );
  }
  strip.show();
  animationPos = millis();
  animationCounter++;
}

// mode 4
void modeConnect(){
  // all red
  strip.fill(strip.Color(0,250,50), 0, NUM_LEDS);
  strip.setPixelColor(animationCounter, strip.Color(250,250,10));
  strip.setPixelColor((NUM_LEDS - animationCounter), strip.Color(250,250,10));
  strip.show();
  animationCounter++;
  if(animationCounter > NUM_LEDS){
    animationCounter = 0;
  }
  animationPos = millis();
}
// mode 5
void modeRunning(){
  strip.fill(strip.Color(0,0,0), 0, NUM_LEDS);
  for(int t = 0; t < 12; t++){
    int rRed = random(0,254);
    int rBlue = random(0,250);
    int rGreen = random(0,240);
    strip.setPixelColor(modeHelper + t, strip.Color( rGreen, rRed, rBlue ) );
  }
  strip.show();
  modeHelper++;
  if(modeHelper > NUM_LEDS){
    modeHelper = -11;
  }
  animationPos = millis();
}

// mode 6
void modeRandomGolden(){
  int rndColorIndex = random(0,goldenArrayLength);
  strip.fill(goldenArray[rndColorIndex],0,NUM_LEDS);
  strip.show();
  animationPos = millis();
}

// mode 7
void modeGoldenShower(){
  strip.fill(strip.Color(0,0,0),0,NUM_LEDS);
  
  if(modeHelper <= 0 ){
    modeHelper = (NUM_LEDS / 2);
  }
  int startLeft = modeHelper;
  int startRight = NUM_LEDS - modeHelper;
  for(int i = 0; i < goldenArrayLength; i++){
    strip.setPixelColor((startLeft - i), goldenArray[i]);
    strip.setPixelColor((startRight + i), goldenArray[i]);
  }
  modeHelper--;
  strip.show();
  animationPos = millis();
}

// mode 8
void modeGoldMaker(){
  int pixi = random(0,NUM_LEDS);
  int goldColor = random(0,goldenArrayLength);
  strip.setPixelColor(pixi, goldenArray[goldColor]);
  strip.show();
  animationPos = millis();
}

// mode 9
void modeRandomFull(){
  int r = random(0,254);
  int g = random(0,254);
  int b = random(0,254);
  strip.fill(strip.Color(g,r,b), 0, NUM_LEDS);
  strip.show();
  animationPos = millis() + 15L;
}


void checkButton(){
  int buttonMode = analogRead(ModeButton);
  if(buttonMode <= 200){
  
    if(buttonCycles < buttonMaxCycles){
      buttonCycles++;
    }
    else{
      // button is pressed
      if(!isButtonPressed && !isChanged){
        Serial.print("Button pressed !");
        Serial.println(isButtonPressed);
        setAnimationType();
        isButtonPressed = true;
      }
    }
  }
  else{
    if(buttonCycles > 0 && isButtonPressed && isChanged){
      // button relased
      Serial.println("Button released");
      Serial.println(buttonMode, DEC);
      buttonCycles = 0;
      isButtonPressed = false;
      isChanged = false;
    }
  }
}


void setAnimationType(){
  if(!isChanged){
    strip.fill(strip.Color(0,0,0), 0, NUM_LEDS);
    animationType++;
    animationCounter = 0;
    modeHelper = 0;
    if(animationType > 9){
      animationType = 0;
    }
    Serial.print("AnimationType: ");
    Serial.println(animationType, DEC);
    isChanged = true;
  }
}
